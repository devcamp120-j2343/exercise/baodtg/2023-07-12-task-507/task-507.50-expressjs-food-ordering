//Khởi tạo thư viện express:
const express = require('express');
const path = require('path');

//Khởi tạo express app:
const app = express();

//Khởi tạo cổng port:
const port  = 8000;


app.use(express.static(__dirname + "/views"));


app.get('/', (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/mainPage.html"));

})
app.get('/cart', (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/cart.html"));

})

app.get('/foods', (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/mainPage.html"));

})

app.listen(port, () => {
    console.log(`App listen on port: ${port}`)
})
